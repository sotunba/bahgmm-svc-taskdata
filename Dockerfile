#   Taken from https://spring.io/guides/gs/spring-boot-docker/
#	FROM:  pulls the latest build from the official postgres images list
#	MAINTAINER:  the keeper of the file

FROM maven:alpine
MAINTAINER gehly_jeffrey@bah.com

WORKDIR /code

# Prepare by downloading dependencies
ADD pom.xml /code/pom.xml
RUN ["mvn", "dependency:resolve"]
RUN ["mvn", "verify"]

# Adding source, compile and package into a fat jar
ADD src /code/src
RUN ["mvn", "package"]

# Set environment variables
ENV JAVA_OPTS="-Dserver.port=8402 -Dspring.data.mongodb.host=192.168.99.101 -Dspring.data.mongodb.port=32768 -Dspring.data.mongodb.database=applications"

# Expose port 8402, which is what we'll need to run this thing off of
EXPOSE 8402

# Set the entrypoint now
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /code/target/utility_service-0.0.1.jar"]
