package taskdata.controller;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ComponentScan(basePackages="taskdata")
public class TaskDataController {

	@RequestMapping(value="/getHello", method=RequestMethod.GET)
	public @ResponseBody String getHello() {
		return "Hello World";
	}
	
}
