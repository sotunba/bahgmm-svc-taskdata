package taskdata.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * This is the main service class for interacting with the data store
 * 		it will either read from the properties file, or initialize
 * 		the connection based upon what it finds
 * 
 * @author 552442
 *
 */
@Component
@ComponentScan("taskdata")
@Service
@EnableMongoRepositories(basePackages = "taskdata.data")
public class TaskDataService {
		
}
